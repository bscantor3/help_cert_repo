-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: bglhslrdinfevlhosud1-mysql.services.clever-cloud.com:3306
-- Generation Time: Jun 21, 2021 at 05:05 AM
-- Server version: 8.0.15-5
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bglhslrdinfevlhosud1`
--

-- --------------------------------------------------------

--
-- Table structure for table `certifications`
--

CREATE TABLE `certifications` (
  `id` mediumint(8) UNSIGNED NOT NULL COMMENT 'Primary key',
  `fk_contract` mediumint(8) UNSIGNED NOT NULL COMMENT 'Foreign key del contrato',
  `fk_type_certification` tinyint(3) UNSIGNED NOT NULL COMMENT 'Foreign key del tipo de certificado',
  `file_path` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'Ruta de acceso del archivo del certificado PDF en el servidor',
  `salary` tinyint(1) NOT NULL COMMENT 'Boolean para mostrar o no mostrar el salario en el certificado',
  `num_exp` tinyint(3) UNSIGNED NOT NULL COMMENT 'Número de expediciones',
  `created_at` timestamp NOT NULL COMMENT 'Fecha y hora de creación del Registro',
  `updated_at` timestamp NOT NULL COMMENT 'Fecha y hora de actualización del Registro'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` tinyint(3) UNSIGNED NOT NULL COMMENT 'Primary key',
  `name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Nombre de la Ciudad'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`) VALUES
(1, 'Armenia'),
(2, 'Barrancabermeja'),
(3, 'Barranquilla'),
(4, 'Bogotá'),
(5, 'Bucaramanga'),
(6, 'Buga'),
(7, 'Cali'),
(8, 'Cartagena'),
(9, 'Chía'),
(10, 'Cúcuta'),
(11, 'Duitama'),
(12, 'Florencia'),
(13, 'Fusagasugá'),
(14, 'Girardot'),
(15, 'Honda'),
(16, 'Ibagué'),
(17, 'Ipiales'),
(18, 'Jamundí'),
(19, 'Leticia'),
(20, 'Manizales'),
(21, 'Mariquita'),
(22, 'Medellín'),
(23, 'Mompox'),
(24, 'Montería'),
(25, 'Murillo'),
(26, 'Neiva'),
(27, 'Pamplona'),
(28, 'Pasto'),
(29, 'Pereira'),
(30, 'Popayán'),
(31, 'Riohacha'),
(32, 'San Andrés y Providencia'),
(33, 'San Antero'),
(34, 'Santa Marta'),
(35, 'Santafé de Antioquia'),
(36, 'San Antero'),
(37, 'Sevilla'),
(38, 'Sincelejo'),
(39, 'Sogamoso'),
(40, 'Tabio'),
(41, 'Tocancipá'),
(42, 'Tolú'),
(43, 'Tuluá'),
(44, 'Tumaco'),
(45, 'Tunja'),
(46, 'Valledupar'),
(47, 'Villavicencio'),
(48, 'Yopal'),
(49, 'Zipaquirá ');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` tinyint(3) UNSIGNED NOT NULL COMMENT 'Primary key',
  `fk_city` tinyint(3) UNSIGNED NOT NULL COMMENT 'Foreign Key City',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Nombre de la institución o empresa',
  `alias` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Abreviatura de la institución o empresa'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `fk_city`, `name`, `alias`) VALUES
(86, 4, 'Servicio Nacional de Aprendizaje – Centro de Electricidad, Electrónica y Telecomunicaciones', 'SENA CEET');

-- --------------------------------------------------------

--
-- Table structure for table `contracts`
--

CREATE TABLE `contracts` (
  `id` mediumint(8) UNSIGNED NOT NULL COMMENT 'Primary key',
  `fk_user` mediumint(8) UNSIGNED NOT NULL COMMENT 'Foreign key del funcionario',
  `fk_job_functions` mediumint(8) UNSIGNED NOT NULL COMMENT 'Foreign key de las funciones del cargo',
  `fk_labor_contract` tinyint(3) UNSIGNED NOT NULL COMMENT 'Foreign key del tipo de contrato labor',
  `contract_term` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Vigencia del contrato',
  `start_date` date NOT NULL COMMENT 'Fecha de inicio del contrato',
  `end_date` date NOT NULL COMMENT 'Fecha de fin del contrato',
  `salary` double UNSIGNED NOT NULL COMMENT 'Valor numérico del salario dispuesto en el contrato',
  `file_path` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'Ruta de acceso del archivo de escaneo del contrato en el servidor',
  `specific_functions` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'Funciones específicas y únicas para el contrato especificado',
  `status` tinyint(1) NOT NULL COMMENT 'Estado de actividad del registro',
  `reason_status` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Motivo de estado de actividad en la plataforma',
  `created_at` timestamp NOT NULL COMMENT 'Fecha y hora de creación del registro',
  `udpated_at` timestamp NOT NULL COMMENT 'Fecha y hora de actualización del registro'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contract_amendments`
--

CREATE TABLE `contract_amendments` (
  `id` mediumint(8) UNSIGNED NOT NULL COMMENT 'Primary key',
  `fk_contract` mediumint(8) UNSIGNED NOT NULL COMMENT 'Foreign key del contrato',
  `new_functions` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'Especificación de nuevas funciones',
  `new_contract_term` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Especificación de nuevas funciones',
  `end_date` date DEFAULT NULL COMMENT 'Especificación de nueva fecha fin del contrato',
  `file_path` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'Ruta de acceso del archivo del nuevo escaneo de contrato',
  `remarks` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'Observaciones del otro sí',
  `created_at` timestamp NOT NULL COMMENT 'Fecha y hora de creación del registro'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `identity_documents`
--

CREATE TABLE `identity_documents` (
  `id` tinyint(3) UNSIGNED NOT NULL COMMENT 'Primary key',
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Nombre del tipo de documento',
  `abbreviation` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Abreviatura del tipo de documento'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `identity_documents`
--

INSERT INTO `identity_documents` (`id`, `name`, `abbreviation`) VALUES
(1, 'Tarjeta de Identidad', 'TI'),
(2, 'Cédula de Ciudadanía', 'CC'),
(3, 'Documento de Identidad', 'DI'),
(4, 'Documento Nacional de Identidad', 'DNI'),
(5, 'Cédula de Extranjería', 'CE'),
(6, 'Tarjeta Pasaporte', 'TP');

-- --------------------------------------------------------

--
-- Table structure for table `job_functions`
--

CREATE TABLE `job_functions` (
  `id` mediumint(8) UNSIGNED NOT NULL COMMENT 'Primary key',
  `fk_company` tinyint(3) UNSIGNED NOT NULL COMMENT 'Foreign key del la institución o empresa',
  `title_job_functions` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Nombre del cargo',
  `contract_object` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Objeto de contrato',
  `specific_obligations` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Obligaciones especificas del cargo',
  `status` tinyint(1) NOT NULL COMMENT 'Estado de actividad del registro',
  `created_at` timestamp NOT NULL COMMENT 'Fecha y Hora de creación del Registro',
  `updated_at` timestamp NOT NULL COMMENT 'Fecha y Hora de actualización del Registro'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `labor_contracts`
--

CREATE TABLE `labor_contracts` (
  `id` tinyint(3) UNSIGNED NOT NULL COMMENT 'Primary key',
  `name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Nombre del tipo de contrato labor',
  `alias` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Alias del tipo de contrato labor'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `labor_contracts`
--

INSERT INTO `labor_contracts` (`id`, `name`, `alias`) VALUES
(18, 'Contrato de Obra o Labor', 'COL'),
(24, 'Contrato a Término Fijo', 'CTF'),
(63, 'Contrato Ocasional de Trabajo', 'COT'),
(93, 'Contrato a Término Indefinido', 'CTI'),
(102, 'Contrato Civil o por Prestación de Servicios', 'CPS'),
(123, 'Contrato de Aprendizaje', 'CA');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` tinyint(3) UNSIGNED NOT NULL COMMENT 'Primary key',
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Nombre del rol en la plataforma',
  `abbreviation` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Abreviatura del rol en la plataforma'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `abbreviation`) VALUES
(16, 'Admin de Admin', 'A'),
(24, 'Team Controller', 'T'),
(38, 'Funcionario', 'F');

-- --------------------------------------------------------

--
-- Table structure for table `types_certification`
--

CREATE TABLE `types_certification` (
  `id` tinyint(3) UNSIGNED NOT NULL COMMENT 'Primary key',
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Nombre del tipo de certificado',
  `alias` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Alias del tipo de certificado'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `types_certification`
--

INSERT INTO `types_certification` (`id`, `name`, `alias`) VALUES
(58, 'Certificado Laboral', 'CL'),
(119, 'Certificado por Funciones', 'CF');

-- --------------------------------------------------------

--
-- Table structure for table `types_of_workday`
--

CREATE TABLE `types_of_workday` (
  `id` tinyint(3) UNSIGNED NOT NULL COMMENT 'Primary key',
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Nombre de la jornada',
  `abbreviation` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Abreviatura de la jornada'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `types_of_workday`
--

INSERT INTO `types_of_workday` (`id`, `name`, `abbreviation`) VALUES
(1, 'Diurna', 'D'),
(2, 'Nocturna', 'N'),
(3, 'Fin de Semana', 'F'),
(4, 'Mixta', 'M');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` mediumint(8) UNSIGNED NOT NULL COMMENT 'Primary key',
  `fk_role` tinyint(3) UNSIGNED NOT NULL COMMENT 'Foreign key del rol de la plataforma',
  `fk_company` tinyint(3) UNSIGNED NOT NULL COMMENT 'Foreign key del la institución o empresa',
  `fk_identity_document` tinyint(3) UNSIGNED NOT NULL COMMENT 'Foreign key del tipo de documento',
  `fk_city` tinyint(3) UNSIGNED NOT NULL COMMENT 'Foreign key de la ciudad',
  `fk_type_of_workday` tinyint(3) UNSIGNED NOT NULL COMMENT 'Foreign key de la jornada',
  `names` varchar(75) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Especificar nombres del usuario',
  `lastnames` varchar(75) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Especificar apellidos del usuario',
  `document_number` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Especificar el número de documento de identidad',
  `job_title` varchar(75) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Especificar el cargo del usuario',
  `email` varchar(75) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Especificar el correo electrónico en la plataforma',
  `password` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Especificar la contraseña en la plataforma, cifrada con bcrypt',
  `genre` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Especificar la abreviatura de género del usuairo',
  `phone_number` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Especificar el número de teléfono del usuario',
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Especificar la dirección del usuario',
  `status` tinyint(1) NOT NULL COMMENT 'Especificar el estado de actividad del usuario en la plataforma',
  `created_at` timestamp NOT NULL COMMENT 'Fecha y hora de creación del registro',
  `updated_at` timestamp NOT NULL COMMENT 'Fecha y hora de actualización del registro'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fk_role`, `fk_company`, `fk_identity_document`, `fk_city`, `fk_type_of_workday`, `names`, `lastnames`, `document_number`, `job_title`, `email`, `password`, `genre`, `phone_number`, `address`, `status`, `created_at`, `updated_at`) VALUES
(1, 16, 86, 2, 4, 1, 'Claudia Janet', 'Gómez Larrota', '37705691', 'Subdirectora del CEET', 'cygomez@sena.edu.co', '$2a$10$lj56IVIA95r9yi15JWzlMOEk0DB4bmBo9iCIxOea7AE3D/uuS5a8e', 'F', '5461500', 'Av.Cra 30 # 17B-25 sur', 1, '2021-06-20 00:00:00', '2021-06-20 00:00:00'),
(2, 24, 86, 2, 4, 3, 'Brayan Steban', 'Cantor Munevar', '1193512063', 'Instructor de Software', 'bscantor3@sena.edu.co', '$2a$10$adz2uisW/M1zBe94K8w1dOksn6SoHvLFWFd6H6ct2c4908D6iL2Rq', 'M', '3213208196', 'Kra 73 I No 62 A 46 sur Galicia', 1, '2021-06-20 23:22:00', '2021-06-20 23:28:42'),
(3, 38, 86, 2, 4, 3, 'Carmen Omaira', 'Munevar Guatavita', '39640824', 'Apoyo Bienestar', 'maya@sena.edu.co', '$2a$10$.seHKs6w9VLG9LqmQzf8w.WSETILCxMvG8byc0tnejkXcRlKb./oO', 'F', '3212042514', 'Kra 73 I No 62 A 46 sur Galicia', 0, '2021-06-20 23:26:02', '2021-06-20 23:27:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `certifications`
--
ALTER TABLE `certifications`
  ADD PRIMARY KEY (`id`,`fk_contract`,`fk_type_certification`),
  ADD KEY `fk_certifications_contracts1_idx` (`fk_contract`),
  ADD KEY `fk_certifications_types_certification1_idx` (`fk_type_certification`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`,`fk_city`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`),
  ADD UNIQUE KEY `alias_UNIQUE` (`alias`),
  ADD KEY `fk_companies_cities1_idx` (`fk_city`);

--
-- Indexes for table `contracts`
--
ALTER TABLE `contracts`
  ADD PRIMARY KEY (`id`,`fk_user`,`fk_job_functions`,`fk_labor_contract`),
  ADD KEY `fk_contracts_users1_idx` (`fk_user`),
  ADD KEY `fk_contracts_job_functions1_idx` (`fk_job_functions`),
  ADD KEY `fk_contracts_labor_contracts1_idx` (`fk_labor_contract`);

--
-- Indexes for table `contract_amendments`
--
ALTER TABLE `contract_amendments`
  ADD PRIMARY KEY (`id`,`fk_contract`),
  ADD KEY `fk_contract_amendments_contracts1_idx` (`fk_contract`);

--
-- Indexes for table `identity_documents`
--
ALTER TABLE `identity_documents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`),
  ADD UNIQUE KEY `abbreviation_UNIQUE` (`abbreviation`);

--
-- Indexes for table `job_functions`
--
ALTER TABLE `job_functions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_job_functions_companies_idx` (`fk_company`);

--
-- Indexes for table `labor_contracts`
--
ALTER TABLE `labor_contracts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias_UNIQUE` (`alias`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`),
  ADD UNIQUE KEY `abbreviation_UNIQUE` (`abbreviation`);

--
-- Indexes for table `types_certification`
--
ALTER TABLE `types_certification`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indexes for table `types_of_workday`
--
ALTER TABLE `types_of_workday`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `abbreviation_UNIQUE` (`abbreviation`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`,`fk_role`,`fk_company`,`fk_identity_document`,`fk_city`,`fk_type_of_workday`),
  ADD UNIQUE KEY `document_number_UNIQUE` (`document_number`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD KEY `fk_users_roles_idx` (`fk_role`),
  ADD KEY `fk_users_companies1_idx` (`fk_company`),
  ADD KEY `fk_users_identity_documents1_idx` (`fk_identity_document`),
  ADD KEY `fk_users_cities1_idx` (`fk_city`),
  ADD KEY `fk_users_types_of_workday1_idx` (`fk_type_of_workday`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary key', AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `identity_documents`
--
ALTER TABLE `identity_documents`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary key', AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `types_of_workday`
--
ALTER TABLE `types_of_workday`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary key', AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `certifications`
--
ALTER TABLE `certifications`
  ADD CONSTRAINT `fk_certifications_contracts1` FOREIGN KEY (`fk_contract`) REFERENCES `contracts` (`id`),
  ADD CONSTRAINT `fk_certifications_types_certification1` FOREIGN KEY (`fk_type_certification`) REFERENCES `types_certification` (`id`);

--
-- Constraints for table `companies`
--
ALTER TABLE `companies`
  ADD CONSTRAINT `fk_companies_cities1` FOREIGN KEY (`fk_city`) REFERENCES `cities` (`id`);

--
-- Constraints for table `job_functions`
--
ALTER TABLE `job_functions`
  ADD CONSTRAINT `fk_job_functiones_companies1` FOREIGN KEY (`fk_company`) REFERENCES `companies` (`id`);

--
-- Constraints for table `contracts`
--
ALTER TABLE `contracts`
  ADD CONSTRAINT `fk_contracts_job_functions1` FOREIGN KEY (`fk_job_functions`) REFERENCES `job_functions` (`id`),
  ADD CONSTRAINT `fk_contracts_labor_contracts1` FOREIGN KEY (`fk_labor_contract`) REFERENCES `labor_contracts` (`id`),
  ADD CONSTRAINT `fk_contracts_users1` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`);

--
-- Constraints for table `contract_amendments`
--
ALTER TABLE `contract_amendments`
  ADD CONSTRAINT `fk_contract_amendments_contracts1` FOREIGN KEY (`fk_contract`) REFERENCES `contracts` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_cities1` FOREIGN KEY (`fk_city`) REFERENCES `cities` (`id`),
  ADD CONSTRAINT `fk_users_companies1` FOREIGN KEY (`fk_company`) REFERENCES `companies` (`id`),
  ADD CONSTRAINT `fk_users_identity_documents1` FOREIGN KEY (`fk_identity_document`) REFERENCES `identity_documents` (`id`),
  ADD CONSTRAINT `fk_users_roles` FOREIGN KEY (`fk_role`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `fk_users_types_of_workday1` FOREIGN KEY (`fk_type_of_workday`) REFERENCES `types_of_workday` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
