<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:include page="/pages/canvas/header.jsp" />
<jsp:include page="/pages/canvas/nav.jsp" />

<div class="container-fluid content-container bg-white">
    <div class="row">
        <div class="col-3 aside-menu">
            <jsp:include page="/pages/dashboards/components/admin_panel.jsp" />
        </div>
        <div class="col-9 principal-content px-5">
            <jsp:include page="/pages/canvas/breadcrumb.jsp" />
            <div class="pt-2">
                <div class="row justify-content-center align-items-center">
                    <jsp:include page="/pages/canvas/search.jsp" />
                    <div class="col-md-2 offset-md-1">
                        <div class="input-group">
                            <button class="btn-d2 p-2 shadow">CREAR</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-m mt-3 pr-3 custom-scroll">
                <table class="table">
                    <thead class="thead-dark">
                        <tr class="text-center">
                            <th scope="col">ID</th>
                            <th scope="col">Tipo_Doc</th>
                            <th scope="col">Documento</th>
                            <th scope="col">Apellidos</th>
                            <th scope="col">Nombres</th>
                            <th scope="col">Jornada</th>
                            <th scope="col">Estado</th>
                            <th scope="col"><i class="fas text-white fa-eye icons-standar"></i></th>
                            <th scope="col"><i class="fas text-white fa-marker"></i></th></th>
                            <th scope="col"><i class="fas text-white fa-check-circle"></i></th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="user" items="${users}">
                            <tr>
                                <td>${user.id}</td>
                                <td>${user.identity_document.abbreviation}</td>
                                <td>${user.document_number}</td>
                                <td>${user.lastnames}</td>
                                <td>${user.names}</td>
                                <td>${user.workday}</td>
                                <td>${user.status}</td>
                                <td><a class="icono-m-d d-flex justify-content-center align-items-center mx-1" href=""
                                       ><i class="fas text-white fa-eye"></i
                                        ></a></td> 
                                <td>	<a class="icono-m-d d-flex justify-content-center align-items-center mx-1" href=""
                                        ><i class="fas text-white fa-marker"></i
                                        ></a></td>
                                <td><a class="icono-m-d d-flex justify-content-center align-items-center mx-1" href=""
                                       ><i class="fas text-white fa-trash-alt"></i
                                        ></a></td>
                            </tr>
                        </c:forEach>

                    </tbody>
                </table>
            </div>
        </div>
        <jsp:include page="/pages/canvas/footer.jsp" />
    </div>
</div>
