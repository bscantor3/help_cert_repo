/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.4
 */
package logics;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Credentials;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import static okhttp3.internal.Util.EMPTY_REQUEST;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author bcantor
 */
public class HttpConnection {

    public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(5, TimeUnit.MINUTES)
            .writeTimeout(5, TimeUnit.MINUTES)
            .readTimeout(5, TimeUnit.MINUTES)
            .build();

    public String doAuth(String url, String email, String password) throws IOException {
        String credential = Credentials.basic(email, password);

        Request request = new Request.Builder()
                .url(url)
                .post(EMPTY_REQUEST)
                .header("Authorization", credential)
                .build();
        try ( Response response = client.newCall(request).execute()) {
            String res = (String.valueOf(response.code()) + "|" + response.body().string());
            return res;
        }
    }

    public String doGet(String url, String token) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization", "Bearer " + token)
                .build();

        try ( Response response = client.newCall(request).execute()) {
            String res = (String.valueOf(response.code()) + "|" + response.body().string());
            return res;
        }
    }

    public String doPost(String url, String token, String json) throws IOException {

        RequestBody body;
        body = json == "" ? EMPTY_REQUEST : RequestBody.create(json, JSON);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader("Authorization", "Bearer " + token)
                .build();

        try ( Response response = client.newCall(request).execute()) {
            String res = (String.valueOf(response.code()) + "|" + response.body().string());
            return res;
        }
    }

    public String doPut(String url, String token, String json) throws IOException {
        RequestBody body = RequestBody.create(json, JSON);
        Request request = new Request.Builder()
                .url(url)
                .put(body)
                .addHeader("Authorization", "Bearer " + token)
                .build();
        try ( Response response = client.newCall(request).execute()) {
            String res = (String.valueOf(response.code()) + "|" + response.body().string());
            return res;
        }
    }

    public String doDelete(String url, String token) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .delete()
                .addHeader("Authorization", "Bearer " + token)
                .build();
        try ( Response response = client.newCall(request).execute()) {
            String res = (String.valueOf(response.code()) + "|" + response.body().string());
            return res;
        }
    }

}
