/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logics;

/**
 *
 * @author genyu
 */
public class ServiceFactory {

    private final String BackendURLServer = "https://help-cert-services.herokuapp.com/api/v2";

    private final String auth = "/auth";
    private final String user = "/users";
    private final String info = "/info";
    private final String profile = "/profile";
    private final String job = "/jobs";
    private final String contract = "/contracts";
    private final String certification = "/certifications";

    public ServiceFactory() {
    }

    public String getUrl(String service) {

        String url = "";

        switch (service) {
            case "auth":
                url = BackendURLServer + auth;
                break;
            case "user":
                url = BackendURLServer + user;
                break;
            case "info":
                url = BackendURLServer + info;
                break;
            case "profile":
                url = BackendURLServer + profile;
                break;
            case "job":
                url = BackendURLServer + job;
                break;
            case "contract":
                url = BackendURLServer + contract;
                break;
            case "certification":
                url = BackendURLServer + certification;
                break;

        }

        return url;
    }

    public String getBackendURLServer() {
        return BackendURLServer;
    }

    public String getAuth() {
        return auth;
    }

    public String getUser() {
        return user;
    }

    public String getInfo() {
        return info;
    }

    public String getProfile() {
        return profile;
    }

    public String getJob() {
        return job;
    }

    public String getContract() {
        return contract;
    }

    public String getCertification() {
        return certification;
    }

}
