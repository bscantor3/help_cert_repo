/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author genyu
 */
public class Identity_Document {

    private int id;
    private String name;
    private String abbreviation;

    public Identity_Document() {
    }

    public Identity_Document(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public Identity_Document(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Identity_Document(int id, String name, String abbreviation) {
        this.id = id;
        this.name = name;
        this.abbreviation = abbreviation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

}
