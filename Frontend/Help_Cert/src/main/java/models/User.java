/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author genyu
 */
public class User {

    private int id;
    private Role rol;
    private Company company;
    private Identity_Document identity_document;
    private City city;
    private Workday workday;
    private String names;
    private String lastnames;
    private String document_number;
    private String job_title;
    private String email;
    private String password;
    private String genre;
    private String phone_number;
    private String address;
    private int status;
    private String status_text;
    private String created_at;
    private String updated_at;
    private int num_contracts;

    public User() {
    }

    public User(int id, Role rol, Company company, String names, String lastnames, String email, String password, int status) {
        this.id = id;
        this.rol = rol;
        this.company = company;
        this.names = names;
        this.lastnames = lastnames;
        this.email = email;
        this.password = password;
        this.status = status;
    }

    public User(int id, Identity_Document identity_document, Workday workday, String names, String lastnames, String document_number, String status_text) {
        this.id = id;
        this.identity_document = identity_document;
        this.workday = workday;
        this.names = names;
        this.lastnames = lastnames;
        this.document_number = document_number;
        this.status_text = status_text;
    }

    public User(int id, Role rol, Identity_Document identity_document, City city, Workday workday, String names, String lastnames, String document_number, String job_title, String genre, String phone_number, String address, String status_text, String created_at, String updated_at, int num_contracts) {
        this.id = id;
        this.rol = rol;
        this.identity_document = identity_document;
        this.city = city;
        this.workday = workday;
        this.names = names;
        this.lastnames = lastnames;
        this.document_number = document_number;
        this.job_title = job_title;
        this.genre = genre;
        this.phone_number = phone_number;
        this.address = address;
        this.status_text = status_text;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.num_contracts = num_contracts;
    }
    
    

    public User(int id, Role rol, Company company, Identity_Document identity_document, City city, Workday workday, String names, String lastnames, String document_number, String job_title, String email, String password, String genre, String phone_number, String address, int status, String status_text, String created_at, String updated_at) {
        this.id = id;
        this.rol = rol;
        this.company = company;
        this.identity_document = identity_document;
        this.city = city;
        this.workday = workday;
        this.names = names;
        this.lastnames = lastnames;
        this.document_number = document_number;
        this.job_title = job_title;
        this.email = email;
        this.password = password;
        this.genre = genre;
        this.phone_number = phone_number;
        this.address = address;
        this.status = status;
        this.status_text = status_text;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Role getRol() {
        return rol;
    }

    public void setRol(Role rol) {
        this.rol = rol;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Identity_Document getIdentity_document() {
        return identity_document;
    }

    public void setIdentity_document(Identity_Document identity_document) {
        this.identity_document = identity_document;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Workday getWorkday() {
        return workday;
    }

    public void setWorkday(Workday workday) {
        this.workday = workday;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getLastnames() {
        return lastnames;
    }

    public void setLastnames(String lastnames) {
        this.lastnames = lastnames;
    }

    public String getDocument_number() {
        return document_number;
    }

    public void setDocument_number(String document_number) {
        this.document_number = document_number;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatus_text() {
        return status_text;
    }

    public void setStatus_text(String status_text) {
        this.status_text = status_text;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

}
