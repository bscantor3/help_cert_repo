/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Date;

/**
 *
 * @author genyu
 */
public class Amendment {

    private int id;
    private Contract contract;
    private String new_functions;
    private String new_contract_term;
    private String end_date;
    private String file_path;
    private String remarks;
    private String created_at;

    public Amendment() {
    }

    public Amendment(int id, Contract contract, String new_functions, String new_contract_term, String end_date, String file_path, String remarks, String created_at) {
        this.id = id;
        this.contract = contract;
        this.new_functions = new_functions;
        this.new_contract_term = new_contract_term;
        this.end_date = end_date;
        this.file_path = file_path;
        this.remarks = remarks;
        this.created_at = created_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public String getNew_functions() {
        return new_functions;
    }

    public void setNew_functions(String new_functions) {
        this.new_functions = new_functions;
    }

    public String getNew_contract_term() {
        return new_contract_term;
    }

    public void setNew_contract_term(String new_contract_term) {
        this.new_contract_term = new_contract_term;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

}
