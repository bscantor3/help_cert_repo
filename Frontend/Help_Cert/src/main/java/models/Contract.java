/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author genyu
 */
public class Contract {

    private int id;
    private User user;
    private Job job;
    private Labor_Contract labor_contract;
    private String contract_term;
    private String start_date;
    private String end_date;
    private double salary;
    private String file_path;
    private String specific_functions;
    private int status;
    private String status_text;
    private String reason_status;
    private String created_at;
    private String updated_at;
    private ArrayList<Amendment> contract_amendments;

    public Contract() {
    }

    public Contract(int id, User user, Job job, Labor_Contract labor_contract, String contract_term, String start_date, String end_date, double salary, String file_path, String specific_functions, int status, String status_text, String reason_status, String created_at, String updated_at) {
        this.id = id;
        this.user = user;
        this.job = job;
        this.labor_contract = labor_contract;
        this.contract_term = contract_term;
        this.start_date = start_date;
        this.end_date = end_date;
        this.salary = salary;
        this.file_path = file_path;
        this.specific_functions = specific_functions;
        this.status = status;
        this.status_text = status_text;
        this.reason_status = reason_status;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Labor_Contract getLabor_contract() {
        return labor_contract;
    }

    public void setLabor_contract(Labor_Contract labor_contract) {
        this.labor_contract = labor_contract;
    }

    public String getContract_term() {
        return contract_term;
    }

    public void setContract_term(String contract_term) {
        this.contract_term = contract_term;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getSpecific_functions() {
        return specific_functions;
    }

    public void setSpecific_functions(String specific_functions) {
        this.specific_functions = specific_functions;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatus_text() {
        return status_text;
    }

    public void setStatus_text(String status_text) {
        this.status_text = status_text;
    }

    public String getReason_status() {
        return reason_status;
    }

    public void setReason_status(String reason_status) {
        this.reason_status = reason_status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public ArrayList<Amendment> getContract_amendments() {
        return contract_amendments;
    }

    public void setContract_amendments(ArrayList<Amendment> contract_amendments) {
        this.contract_amendments = contract_amendments;
    }

}
