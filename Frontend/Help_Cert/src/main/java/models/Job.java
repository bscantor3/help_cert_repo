/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author genyu
 */
public class Job {

    private int id;
    private Company company;
    private String title_job_functions;
    private String contract_object;
    private String specific_obligations;
    private int status;
    private String status_text;
    private String created_at;
    private String updated_at;

    public Job() {
    }

    public Job(int id, Company company, String title_job_functions, String contract_object, String specific_obligations, int status, String status_text, String created_at, String updated_at) {
        this.id = id;
        this.company = company;
        this.title_job_functions = title_job_functions;
        this.contract_object = contract_object;
        this.specific_obligations = specific_obligations;
        this.status = status;
        this.status_text = status_text;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getTitle_job_functions() {
        return title_job_functions;
    }

    public void setTitle_job_functions(String title_job_functions) {
        this.title_job_functions = title_job_functions;
    }

    public String getContract_object() {
        return contract_object;
    }

    public void setContract_object(String contract_object) {
        this.contract_object = contract_object;
    }

    public String getSpecific_obligations() {
        return specific_obligations;
    }

    public void setSpecific_obligations(String specific_obligations) {
        this.specific_obligations = specific_obligations;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatus_text() {
        return status_text;
    }

    public void setStatus_text(String status_text) {
        this.status_text = status_text;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

}
