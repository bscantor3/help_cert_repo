/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author genyu
 */
public class Certification {

    private int id;
    private Contract contract;
    private Type_Certification type_certification;
    private String file_path;
    private int num_exp;
    private String created_at;
    private String updated_at;

    public Certification() {
    }

    public Certification(int id, Contract contract, Type_Certification type_certification, String file_path, int num_exp, String created_at, String updated_at) {
        this.id = id;
        this.contract = contract;
        this.type_certification = type_certification;
        this.file_path = file_path;
        this.num_exp = num_exp;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public Type_Certification getType_certification() {
        return type_certification;
    }

    public void setType_certification(Type_Certification type_certification) {
        this.type_certification = type_certification;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public int getNum_exp() {
        return num_exp;
    }

    public void setNum_exp(int num_exp) {
        this.num_exp = num_exp;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

}
