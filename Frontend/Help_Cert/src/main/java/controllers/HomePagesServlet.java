/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import daos.AuthDao;
import daos.InfoDao;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import logics.ServletHandling;
import models.Company;
import models.Message;
import models.Role;
import models.Session;
import models.User;
import okhttp3.Response;
import org.json.JSONObject;

/**
 *
 * @author genyu
 */
public class HomePagesServlet extends HttpServlet {

    private static String path = "pages/homepages/";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String page = request.getParameter("p");
        ServletHandling servletLogic = new ServletHandling(request, response);
        servletLogic.configPathJSP(0, path, page);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        String action = request.getParameter("a");

        if (action.equals("signin")) {
            HttpSession session = request.getSession();
            session.setMaxInactiveInterval(60 * 10);

            String email = request.getParameter("email");
            String password = request.getParameter("password");

            AuthDao dao = new AuthDao();
            try {
                JSONObject JSON = dao.signInMethod(email, password);
                Message message = new Message(JSON.getInt("http"), JSON.getString("code"), JSON.getString("message"));

                if (message.getHttpcode() == 401) {
                    response.sendRedirect(request.getContextPath() + "/home?p=signin");
                }

                if (message.getHttpcode() == 200) {
                    JSONObject data = JSON.getJSONObject("data");
                    JSONObject tokens = JSON.getJSONObject("tokens");

                    Session info = new Session(tokens.getString("refreshToken"),
                            tokens.getString("accessToken"),
                            new User(
                                    data.getInt("id"),
                                    new Role(data.getInt("fk_role"), data.getString("company")),
                                    new Company(data.getInt("fk_company"), data.getString("company")),
                                    data.getString("names"),
                                    data.getString("lastnames"),
                                    data.getString("email"),
                                    data.getString("password"),
                                    data.getInt("status")
                            )
                    );

                    session.setAttribute("session", info);

                    InfoDao daoInfo = new InfoDao(info.getAccessToken(), info.getRefreshToken());
                    JSONObject JSONcards = daoInfo.cardsMethod();
                    Message messagecards = new Message(JSONcards.getInt("http"), JSONcards.getString("code"), JSONcards.getString("message"));

                    if (messagecards.getHttpcode() == 401) {
                        response.sendRedirect(request.getContextPath() + "/home?p=signin");
                    }

                    if (messagecards.getHttpcode() == 200) {
                        Object datacards = JSONcards.getJSONArray("data").get(0);
                        session.setAttribute("cards", datacards);
                        response.sendRedirect(request.getContextPath() + "/my-home/dashboard");
                    }

                }

            } catch (Exception ex) {
                Logger.getLogger(HomePagesServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }
}
