/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import daos.AuthDao;
import daos.InfoDao;
import daos.UserDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import logics.ServletHandling;
import models.City;
import models.Company;
import models.Identity_Document;
import models.Message;
import models.Session;
import models.Role;
import models.Session;
import models.User;
import models.Workday;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author genyu
 */
public class FunctionaryServlet extends HttpServlet {

    private static String path = "pages/functionaries/";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        if (session.getAttribute("session") == null) {
            response.sendRedirect(request.getContextPath() + "/home?p=signin");
            return;
        }

        String action = request.getParameter("a");

        Session info = (Session) session.getAttribute("session");

        UserDao dao = new UserDao(info.getAccessToken(), info.getRefreshToken());

        if (action.equals("index")) {
            JSONObject JSON = dao.indexMethod(38, "", 1);
            Message message = new Message(JSON.getInt("http"), JSON.getString("code"), JSON.getString("message"));

            System.out.println(JSON);
            if (message.getHttpcode() == 200) {

                ArrayList<User> users = new ArrayList<User>();

                JSONArray arrayJSON = JSON.getJSONArray("data");

                if (arrayJSON != null) {
                    for (int i = 0; i < arrayJSON.length(); i++) {
                        users.add(new User(
                                arrayJSON.getJSONObject(i).getInt("id"),
                                new Identity_Document(
                                        arrayJSON.getJSONObject(i).getString("identity_document")
                                ),
                                new Workday(
                                        arrayJSON.getJSONObject(i).getString("wokday")
                                ),
                                arrayJSON.getJSONObject(i).getString("names"),
                                arrayJSON.getJSONObject(i).getString("lastnames"),
                                arrayJSON.getJSONObject(i).getString("document_number"),
                                arrayJSON.getJSONObject(i).getString("status")
                        ));
                    }
                }

                session.setAttribute("users", users);

                ServletHandling servletLogic = new ServletHandling(request, response);
                servletLogic.configPathJSP(0, path, null);
            }

            if (message.getHttpcode() == 403) {

                if (JSON.getString("code").equals("ERR_EXPIRATE_TOKEN")) {
                    AuthDao daoAuth = new AuthDao();
                    JSONObject accessJSON = daoAuth.refreshMethod(dao.getRefreshToken());
                    if (accessJSON.getString("code").equals("VALID_REFRESH")) {
                        dao.setAccessToken((String) accessJSON.get("accessToken"));
                        JSON = dao.indexMethod(38, "", 1);
                        message = new Message(JSON.getInt("http"), JSON.getString("code"), JSON.getString("message"));
                        if (JSON.getInt("http") != 200) {
                            session.invalidate();
                            response.sendRedirect(request.getContextPath() + "/my-home/dashboard");
                            return;
                        }
                    }
                }

            }
        }

        if (action.equals("info")) {

            int id = Integer.parseInt(request.getParameter("id"));
            int role = Integer.parseInt(request.getParameter("role"));

            JSONObject JSON = dao.indexOfMethod(id, role);
            Message message = new Message(JSON.getInt("http"), JSON.getString("code"), JSON.getString("message"));
            System.out.println("entra");
            System.out.println(JSON);
            if (message.getHttpcode() == 200) {

                User user = new User(
                        JSON.getJSONArray("data").getJSONObject(0).getInt("id"),
                        new Role(
                                JSON.getJSONArray("data").getJSONObject(0).getInt("fk_role")
                        ),
                        new Identity_Document(
                                JSON.getJSONArray("data").getJSONObject(0).getInt("id_identity_document"),
                                JSON.getJSONArray("data").getJSONObject(0).getString("identity_document")
                        ),
                        new City(
                                JSON.getJSONArray("data").getJSONObject(0).getInt("id_city"),
                                JSON.getJSONArray("data").getJSONObject(0).getString("city")
                        ),
                        new Workday(
                                JSON.getJSONArray("data").getJSONObject(0).getInt("id_workday"),
                                JSON.getJSONArray("data").getJSONObject(0).getString("workday")
                        ),
                        JSON.getJSONArray("data").getJSONObject(0).getString("names"),
                        JSON.getJSONArray("data").getJSONObject(0).getString("lastnames"),
                        JSON.getJSONArray("data").getJSONObject(0).getString("document_number"),
                        JSON.getJSONArray("data").getJSONObject(0).getString("job_title"),
                        JSON.getJSONArray("data").getJSONObject(0).getString("genre"),
                        JSON.getJSONArray("data").getJSONObject(0).getString("phone_number"),
                        JSON.getJSONArray("data").getJSONObject(0).getString("address"),
                        JSON.getJSONArray("data").getJSONObject(0).getString("status"),
                        JSON.getJSONArray("data").getJSONObject(0).getString("created_at"),
                        JSON.getJSONArray("data").getJSONObject(0).getString("updated_at"),
                        JSON.getJSONArray("data").getJSONObject(0).getInt("num_contracts")
                );

                session.setAttribute("user", user);

                ServletHandling servletLogic = new ServletHandling(request, response);
                servletLogic.configPathJSP(0, path, "info");
            }
        }

        if (action.equals("create")) {

            InfoDao daoInfo = new InfoDao(info.getAccessToken(), info.getRefreshToken());
            JSONObject JSON = daoInfo.formsMethod("users");
            Message message = new Message(JSON.getInt("http"), JSON.getString("code"), JSON.getString("message"));

            if (message.getHttpcode() == 401) {
                response.sendRedirect(request.getContextPath() + "/home?p=signin");
            }

            if (message.getHttpcode() == 200) {
                Object data = JSON.getJSONObject("data");
                session.setAttribute("forms", data);
                ServletHandling servletLogic = new ServletHandling(request, response);
                servletLogic.configPathJSP(0, path, "create");
            }
        }

        if (action.equals("update")) {
            ServletHandling servletLogic = new ServletHandling(request, response);
            servletLogic.configPathJSP(0, path, "update");
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        String action = request.getParameter("a");

        if (action.equals("create")) {

        }

        if (action.equals("update")) {

        }

        if (action.equals("delete")) {

        }

    }

}
