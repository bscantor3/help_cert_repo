/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import logics.ServletHandling;
import models.Session;

/**
 *
 * @author genyu
 */
public class DashboardServlet extends HttpServlet {

    private static String path = "pages/dashboards/";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        if (session.getAttribute("session") == null) {
            response.sendRedirect(request.getContextPath() + "/home?p=signin");
            return;
        }

        Session info = (Session) session.getAttribute("session");

        int role = info.getUser().getRol().getId();

        ServletHandling servletLogic = new ServletHandling(request, response);

        switch (role) {
            case 16:
                servletLogic.configPathJSP(0, path, "admin_index");
                break;
            case 24:
                servletLogic.configPathJSP(0, path, "team_index");
                break;
            case 38:
                servletLogic.configPathJSP(0, path, "functionary_index");
                break;
        }

    }
}
