/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import java.io.IOException;
import logics.HttpConnection;
import logics.ServiceFactory;
import models.Job;
import models.Response;
import models.User;
import org.json.JSONObject;

/**
 *
 * @author genyu
 */
public class ProfileDao {

    private String url;
    private HttpConnection api;
    private String accessToken;
    private String refreshToken;

    public ProfileDao(String accessToken, String refreshToken) {
        this.url = new ServiceFactory().getUrl("profile");
        this.api = new HttpConnection();
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public JSONObject infoMethod() throws IOException {
        JSONObject body = new JSONObject();

        String jsonString = api.doGet(url + "/", accessToken);
        String[] parts = jsonString.split("\\|");

        if (parts.length == 0) {
            return null;
        } else {
            JSONObject JSON = new JSONObject(String.valueOf(parts[1]));
            JSON.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return JSON;
        }
    }

    public JSONObject updateMethod(User user) throws IOException {
        JSONObject body = new JSONObject();

        body.put("lastnames", user.getLastnames());
        body.put("names", user.getNames());
        body.put("identity_document", user.getIdentity_document());
        body.put("document_number", user.getDocument_number());
        body.put("address", user.getAddress());
        body.put("email", user.getEmail());
        body.put("phone_number", user.getPhone_number());
        body.put("workday", user.getWorkday().getId());
        body.put("job", user.getJob_title());
        body.put("genre", user.getGenre());
        body.put("city", user.getCity().getId());

        String jsonString = api.doPut(url + "/" + user.getId(), accessToken, body.toString());
        String[] parts = jsonString.split("\\|");

        if (parts.length == 0) {
            return null;
        } else {
            JSONObject JSON = new JSONObject(String.valueOf(parts[1]));
            JSON.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return JSON;
        }
    }

    public JSONObject updateCredentialsMethod(String oldPassword, String newPassword) throws IOException {
        JSONObject body = new JSONObject();

        body.put("oldPassword", oldPassword);
        body.put("newPassword", newPassword);

        String jsonString = api.doPut(url + "/credentials", accessToken, body.toString());
        String[] parts = jsonString.split("\\|");

        if (parts.length == 0) {
            return null;
        } else {
            JSONObject JSON = new JSONObject(String.valueOf(parts[1]));
            JSON.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return JSON;
        }
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HttpConnection getApi() {
        return api;
    }

    public void setApi(HttpConnection api) {
        this.api = api;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
