/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import java.io.IOException;
import java.util.ArrayList;
import logics.HttpConnection;
import logics.ServiceFactory;
import models.Message;
import models.User;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author genyu
 */
public class UserDao {

    private String url;
    private HttpConnection api;
    private String accessToken;
    private String refreshToken;

    public UserDao(String accessToken, String refreshToken) {
        this.url = new ServiceFactory().getUrl("user");
        this.api = new HttpConnection();
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public JSONObject createMethod(User user) throws IOException {
        JSONObject body = new JSONObject();

        body.put("fk_role", user.getRol().getId());
        body.put("fk_company", user.getRol().getId());
        body.put("fk_identity_document", user.getRol().getId());
        body.put("fk_city", user.getRol().getId());
        body.put("fk_type_of_workday", user.getRol().getId());
        body.put("names", user.getRol().getId());
        body.put("lastnames", user.getRol().getId());
        body.put("document_number", user.getRol().getId());
        body.put("job_title", user.getRol().getId());
        body.put("email", user.getRol().getId());
        body.put("genre", user.getRol().getId());
        body.put("phone_number", user.getRol().getId());
        body.put("address", user.getRol().getId());

        String jsonString = api.doPost(url + "/create", accessToken, body.toString());
        String[] parts = jsonString.split("\\|");

        if (parts.length == 0) {
            return null;
        } else {
            JSONObject JSON = new JSONObject(String.valueOf(parts[1]));
            JSON.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return JSON;
        }

    }

    public JSONObject indexMethod(int role, String document, int status) throws IOException {
        JSONObject body = new JSONObject();

        body.put("role", role);
        body.put("document", document);
        body.put("status", status);

        String jsonString = api.doPost(url + "/", accessToken, body.toString());
        String[] parts = jsonString.split("\\|");

        if (parts.length == 0) {
            return null;
        } else {
            JSONObject result = new JSONObject(String.valueOf(parts[1]));
            result.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return result;
        }
    }

    public JSONObject indexOfMethod(int id, int role) throws IOException {
        JSONObject body = new JSONObject();

        body.put("role", role);

        String jsonString = api.doPost(url + "/" + id, accessToken, body.toString());
        String[] parts = jsonString.split("\\|");

        if (parts.length == 0) {
            return null;
        } else {
            JSONObject JSON = new JSONObject(String.valueOf(parts[1]));
            JSON.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return JSON;
        }
    }

    public JSONObject updateMethod(User user) throws IOException {
        JSONObject body = new JSONObject();

        body.put("role", user.getRol().getId());
        body.put("lastnames", user.getRol().getId());
        body.put("names", user.getRol().getId());
        body.put("identity_document", user.getRol().getId());
        body.put("document_number", user.getRol().getId());
        body.put("address", user.getRol().getId());
        body.put("phone_number", user.getRol().getId());
        body.put("workday", user.getRol().getId());
        body.put("job", user.getRol().getId());
        body.put("genre", user.getRol().getId());
        body.put("city", user.getRol().getId());

        String jsonString = api.doPut(url + "/" + user.getId(), accessToken, body.toString());
        String[] parts = jsonString.split("\\|");

        if (parts.length == 0) {
            return null;
        } else {
            JSONObject JSON = new JSONObject(String.valueOf(parts[1]));
            JSON.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return JSON;
        }
    }

    public JSONObject updateStatusMethod(int id, int role) throws IOException {
        JSONObject body = new JSONObject();

        body.put("role", role);

        String jsonString = api.doPut(url + "/" + id, accessToken, body.toString());
        String[] parts = jsonString.split("\\|");

        if (parts.length == 0) {
            return null;
        } else {
            JSONObject JSON = new JSONObject(String.valueOf(parts[1]));
            JSON.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return JSON;
        }
    }

    public JSONObject deleteMethod(int id) throws IOException {

        String jsonString = api.doDelete(url + "/" + id, accessToken);
        String[] parts = jsonString.split("\\|");

        if (parts.length == 0) {
            return null;
        } else {
            JSONObject JSON = new JSONObject(String.valueOf(parts[1]));
            JSON.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return JSON;
        }
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HttpConnection getApi() {
        return api;
    }

    public void setApi(HttpConnection api) {
        this.api = api;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

}
