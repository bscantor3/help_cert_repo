/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import java.io.IOException;
import logics.HttpConnection;
import logics.ServiceFactory;
import models.Contract;
import models.Amendment;
import models.Job;
import models.Response;
import org.json.JSONObject;

/**
 *
 * @author genyu
 */
public class ContractDao {

    private String url;
    private HttpConnection api;
    private String accessToken;
    private String refreshToken;

    public ContractDao(String accessToken, String refreshToken) {
        this.url = new ServiceFactory().getUrl("contract");
        this.api = new HttpConnection();
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public JSONObject createMethod(Contract contract) throws IOException {
        JSONObject body = new JSONObject();

        body.put("user", contract.getUser().getId());
        body.put("job", contract.getJob().getId());
        body.put("labor", contract.getLabor_contract().getId());
        body.put("start_date", contract.getStart_date());
        body.put("end_date", contract.getEnd_date());
        body.put("salary", contract.getSalary());
        body.put("specific", contract.getSpecific_functions());

        String jsonString = api.doPost(url + "/create", accessToken, body.toString());
        String[] parts = jsonString.split("\\|");

        if (parts.length != 0) {
            return null;
        } else {
            JSONObject JSON = new JSONObject(String.valueOf(parts[1]));
            JSON.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return JSON;
        }

    }

    public JSONObject amendmentMethod(Amendment amendment) throws IOException {
        JSONObject body = new JSONObject();

        body.put("functions", amendment.getNew_functions());
        body.put("end_date", amendment.getEnd_date());
        body.put("remarks", amendment.getRemarks());
        body.put("path", amendment.getFile_path());

        String jsonString = api.doPost(url + "/amendments", accessToken, body.toString());
        String[] parts = jsonString.split("\\|");

        if (parts.length != 0) {
            return null;
        } else {
            JSONObject JSON = new JSONObject(String.valueOf(parts[1]));
            JSON.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return JSON;
        }

    }

    public JSONObject indexMethod(String code, int status) throws IOException {
        JSONObject body = new JSONObject();

        body.put("code", code);
        body.put("status", status);

        String jsonString = api.doPost(url + "/", accessToken, body.toString());
        String[] parts = jsonString.split("\\|");

        if (parts.length != 0) {
            return null;
        } else {
            JSONObject result = new JSONObject(String.valueOf(parts[1]));
            result.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return result;
        }
    }

    public JSONObject indexOfMethod(int code) throws IOException {
        JSONObject body = new JSONObject();

        String jsonString = api.doGet(url + "/" + code, accessToken);
        String[] parts = jsonString.split("\\|");

        if (parts.length != 0) {
            return null;
        } else {
            JSONObject JSON = new JSONObject(String.valueOf(parts[1]));
            JSON.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return JSON;
        }
    }

    public JSONObject updateStatusMethod(int code, String reason) throws IOException {
        JSONObject body = new JSONObject();

        body.put("reason_status", reason);
        String jsonString = api.doPut(url + "/" + code + "/status", accessToken, body.toString());
        String[] parts = jsonString.split("\\|");

        if (parts.length != 0) {
            return null;
        } else {
            JSONObject JSON = new JSONObject(String.valueOf(parts[1]));
            JSON.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return JSON;
        }
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HttpConnection getApi() {
        return api;
    }

    public void setApi(HttpConnection api) {
        this.api = api;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

}
