/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import java.io.IOException;
import logics.HttpConnection;
import logics.ServiceFactory;
import models.Job;
import models.Response;
import models.User;
import org.json.JSONObject;

/**
 *
 * @author genyu
 */
public class JobDao {

    private String url;
    private HttpConnection api;
    private String accessToken;
    private String refreshToken;

    public JobDao(String accessToken, String refreshToken) {
        this.url = new ServiceFactory().getUrl("job");
        this.api = new HttpConnection();
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public JSONObject createMethod(Job job) throws IOException {
        JSONObject body = new JSONObject();

        body.put("title", job.getTitle_job_functions());
        body.put("object", job.getContract_object());
        body.put("obligations", job.getSpecific_obligations());

        String jsonString = api.doPost(url + "/create", accessToken, body.toString());
        String[] parts = jsonString.split("\\|");

        if (parts.length == 0) {
            return null;
        } else {
            JSONObject JSON = new JSONObject(String.valueOf(parts[1]));
            JSON.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return JSON;
        }

    }

    public JSONObject indexMethod(String code, int status) throws IOException {
        JSONObject body = new JSONObject();

        body.put("code", code);
        body.put("status", status);

        String jsonString = api.doPost(url + "/", accessToken, body.toString());
        String[] parts = jsonString.split("\\|");

        if (parts.length == 0) {
            return null;
        } else {
            JSONObject result = new JSONObject(String.valueOf(parts[1]));
            result.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return result;
        }
    }

    public JSONObject indexOfMethod(int code) throws IOException {
        JSONObject body = new JSONObject();

        String jsonString = api.doGet(url + "/" + code, accessToken);
        String[] parts = jsonString.split("\\|");

        if (parts.length == 0) {
            return null;
        } else {
            JSONObject JSON = new JSONObject(String.valueOf(parts[1]));
            JSON.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return JSON;
        }
    }

    public JSONObject updateMethod(Job job) throws IOException {
        JSONObject body = new JSONObject();

        body.put("title", job.getTitle_job_functions());
        body.put("object", job.getContract_object());
        body.put("obligations", job.getSpecific_obligations());

        String jsonString = api.doPut(url + "/" + job.getId(), accessToken, body.toString());
        String[] parts = jsonString.split("\\|");

        if (parts.length == 0) {
            return null;
        } else {
            JSONObject JSON = new JSONObject(String.valueOf(parts[1]));
            JSON.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return JSON;
        }
    }

    public JSONObject updateStatusMethod(int code) throws IOException {
        JSONObject body = new JSONObject();

        String jsonString = api.doPut(url + "/" + code + "/status", accessToken, "");
        String[] parts = jsonString.split("\\|");

        if (parts.length == 0) {
            return null;
        } else {
            JSONObject JSON = new JSONObject(String.valueOf(parts[1]));
            JSON.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return JSON;
        }
    }

    public JSONObject deleteMethod(int code) throws IOException {

        String jsonString = api.doDelete(url + "/" + code, accessToken);
        String[] parts = jsonString.split("\\|");

        if (parts.length == 0) {
            return null;
        } else {
            JSONObject JSON = new JSONObject(String.valueOf(parts[1]));
            JSON.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return JSON;
        }
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HttpConnection getApi() {
        return api;
    }

    public void setApi(HttpConnection api) {
        this.api = api;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

}
