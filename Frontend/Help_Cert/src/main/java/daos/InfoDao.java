/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import java.io.IOException;
import logics.HttpConnection;
import logics.ServiceFactory;
import models.Response;
import models.User;
import org.json.JSONObject;

/**
 *
 * @author genyu
 */
public class InfoDao {

    private String url;
    private HttpConnection api;
    private String accessToken;
    private String refreshToken;

    public InfoDao(String accessToken, String refreshToken) {
        this.url = new ServiceFactory().getUrl("info");
        this.api = new HttpConnection();
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public JSONObject cardsMethod() throws IOException {

        String jsonString = api.doGet(url + "/cards", accessToken);
        String[] parts = jsonString.split("\\|");

        if (parts.length == 0) {
            return null;
        } else {
            JSONObject JSON = new JSONObject(String.valueOf(parts[1]));
            JSON.put("http", Integer.parseInt(String.valueOf(parts[0])));
            System.out.println("\n\n\n\n");
            System.out.println(JSON);
            return JSON;
        }
    }

    public JSONObject formsMethod(String form) throws IOException {
        JSONObject body = new JSONObject();

        body.put("form", form);

        String jsonString = api.doPost(url + "/forms", accessToken, body.toString());
        String[] parts = jsonString.split("\\|");

        if (parts.length == 0) {
            return null;
        } else {
            JSONObject JSON = new JSONObject(String.valueOf(parts[1]));
            JSON.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return JSON;
        }
    }

}
