/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import java.io.IOException;
import java.util.ArrayList;
import logics.HttpConnection;
import logics.ServiceFactory;
import models.Message;
import okhttp3.Response;
import models.Session;
import models.User;
import okhttp3.OkHttpClient;
import org.json.JSONObject;

/**
 *
 * @author genyu
 */
public class AuthDao {

    private String url;
    private HttpConnection api;

    public AuthDao() {
        this.url = new ServiceFactory().getUrl("auth");
        this.api = new HttpConnection();
    }

    public JSONObject signInMethod(String email, String password) throws IOException, Exception {

        String jsonString = api.doAuth(url + "/signin", email, password);
        String[] parts = jsonString.split("\\|");

        if (parts.length != 0) {
            JSONObject JSON = new JSONObject(String.valueOf(parts[1]));
            JSON.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return JSON;
        } else {
            return null;
        }

    }

    public JSONObject refreshMethod(String refresh) throws IOException {
        String jsonString = api.doPost(url + "/refresh", refresh, "");
        String[] parts = jsonString.split("\\|");

        if (parts.length != 0) {
            JSONObject JSON = new JSONObject(String.valueOf(parts[1]));
            JSON.put("http", Integer.parseInt(String.valueOf(parts[0])));
            return JSON;

        } else {
            return null;
        }
    }
}
